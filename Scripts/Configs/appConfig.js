/**
 * Created by Brajesh Kr Yadav on 6/21/2014.
 */
var APP_CONFIG = {};

APP_CONFIG.PATH = {
    XML_FILE_PATH : "/Xml/",
    RESOURCE_FOLDER : "/App_Resources/",
    VIEW_FOLDER : "/Views/",
    STATIC_FOLDER : "/Views/Static/",
    WEBPAGE_FOLDER : "/Views/WebPages/",
    SERVICE_URL : "http://usphlvm1513.dmzphl.sap.corp:9080/BenchmarkingService/BenchmarkingService.svc/",
    HTML : ".html",
    ASPX : ".aspx",
    IMAGE_PATH : "./Content/assets/images/",
};
