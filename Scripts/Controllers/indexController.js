objIG.config(function($routeProvider,$locationProvider,$logProvider){//debugger;
    //$routeProvider.
    //    when('/home',{template:'Hi Home'}).
    //    when('/about',{template:'Hi About'}).
    //    otherwise({redirectTo:'/Contact',template:'Hi Brajesh Default page'})
    $logProvider.debugEnabled(true);

    $routeProvider.
        when('/Home',
        {
            templateUrl:"Views/WebPages/Welcome.html",
            controller:'welcomeController'
        }).
        when('/AboutUs',
        {
            templateUrl:"Views/WebPages/AboutUs.html",
            controller:'aboutUsController'
        }).
        when('/Training',
        {
            templateUrl:"Views/WebPages/TrainingBatch.html",
            controller:'trainingController'
        }).
        when('/JobPortal',
        {
            templateUrl:"Views/WebPages/jobsPage.html",
            controller:'jobsController'
        }).
        when('/ContactUs',
        {
            templateUrl:"Views/WebPages/ContactUs.html",
            controller:'contactUsController'
        }).
        otherwise({redirectTo:'/Home',template:'Hi Brajesh Default page'});

    //$locationProvider.html5Mode(true);
});


objIG.controller('MainCtrl' ,function($scope,$location,$log){
    $scope.SetRedirect = function(route)
    {
        $location.path(route);
    }
})
