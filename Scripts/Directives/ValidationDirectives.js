/**
 * Created by C5189757 on 7/26/2014.
 */
objIG.directive('validateTextBox',
    function(){
        return{
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, elm, attr, ctrl){

                if (attr.type === 'radio' || attr.type === 'checkbox') return;
                elm.unbind('input').unbind('keydown').unbind('change');

                elm.bind('mouseover',
                    function(){
                        scope.$apply(dovalidation);
                    });
                elm.bind('mouseout',
                    function(){
                        scope.$apply(removeValidation);
                    });

                elm.bind('blur',
                    function(){
                        scope.$apply(changeClass);
                    });
                scope.$on('kickOffValidations', dovalidation);


                function changeClass()
                {
                    if (elm.val() != "")
                        elm.removeClass('ng-dirty ng-invalid ng-invalid-required');

                    else{
                        elm.removeClass().addClass('regTextbox ng-dirty ng-invalid ng-invalid-required');

                    }
                };

                function dovalidation(){

                    if (elm.val() != ""){

                        ctrl.$setValidity('requiredField', true);
                    }

                    else
                    {
                        if (elm.hasClass('ng-dirty'))
                            ctrl.$setValidity('requiredField', false);
                    }

                };

                function removeValidation()
                {
                    ctrl.$setValidity('requiredField', true);
                }
            }
        };

    });
