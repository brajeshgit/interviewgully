/**
 * Created by Brajesh Kr Yadav on 6/21/2014.
 */
objIG.filter('ReverseData', function(data){
    return function(Message) {
        return Message.split("").reverse().join("") + data.Message;
    };
})