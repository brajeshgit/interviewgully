/**
 * Created by C5189757 on 6/21/2014.
 */
objIG.factory('data', function(){
    return {
        Message : 'hey I am ur service'
    };
});

objIG.factory('Details',function(){
    var StuDetails = {};
    StuDetails.Personal = [
        {
            Name : 'Brajesh',
            Profession : 'Software Developer'
        },
        {
            Name : 'Sujeet',
            Profession : 'Mechanical Engg'},
        {
            Name : 'Ashish',
            Profession : 'Consultant'
        },
        {
            Name : 'Rajesh',
            Profession : 'Software Architect'
        },
        {
            Name : 'Manoj',
            Profession : 'Architect Civil'
        },
        {
            Name : 'Rajneesh',
            Profession : 'App Developer'
        }
    ];
    return StuDetails;
})